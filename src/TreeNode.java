import java.util.*;

public class TreeNode {

   private String name;
   private TreeNode firstChild;
   private TreeNode nextSibling;

   TreeNode (String n, TreeNode d, TreeNode r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   }

   public void setName(String name) {
      this.name = name;
   }
   public void setFirstChild(TreeNode firstChild) {
      this.firstChild = firstChild;
   }
   public void setNextSibling(TreeNode nextSibling) {
      this.nextSibling = nextSibling;
   }

   public static TreeNode parsePrefix (String s) {
      //System.out.println(testBalance(s));
      String p = "#parsing";
      if (s.startsWith(p)) {
         s = s.substring(p.length());
      } else if(!(testBalance(s) == 0)) throw new RuntimeException(s + "<-- Uneven number of parentheses");
      if (s.isEmpty() || s.startsWith("\t"))throw new RuntimeException("Empty string not allowed");
      //System.out.println("sisenesin stringiga: " + s);
      StringTokenizer st = new StringTokenizer(s,"(,)",true );
      while (st.hasMoreTokens()) {
         String token = st.nextToken();
         if (token.contains("(") || token.contains(")") || token.contains(",") || token.contains(" ") || token.contains("\t"))
            throw new RuntimeException("Userinput: " + s + "<-- string has to start with alphanumeric nodename");
         TreeNode root = new TreeNode("", null, null);
         s = s.substring(token.length());
         root.setName(token);
         if (!st.hasMoreTokens()) return root;
         if (!s.endsWith(")")) throw new RuntimeException(s + "String must end with a closing braket");
         token = st.nextToken().trim();
         s = s.substring(token.length());
         if (token.equals("(")) {
            int viimaneSulg = s.indexOf(")")+2;
            TreeNode parsed = TreeNode.parsePrefix(p+s);
            root.setFirstChild(parsed);
            //System.out.println("newfirstchild loodud: ");
            if ((viimaneSulg > s.length())) {
               //System.out.println("olen siiin");
               //System.out.println("väljutan: " + root.name + " " + parsed.name + " null");
               return root;
               // return new TreeNode(rootName, newFirstChild, null;
            }

            // String peale lapse leidmist
            //System.out.println("hakkan leidma stringi peale last: " + s);
            int index = 0;
            int openPar = 1;
            for (int i = 0; i < s.length(); i++) {
               //System.out.println(s.charAt(i));
               if (s.charAt(i)==')') openPar--;
               if (s.charAt(i)=='(') openPar++;
               //System.out.println(openPar);
               if (openPar == 0) {
                  index = i;
                  //System.out.println("index tsükkli sees: " + index );
                  break;
               }

            }
            //System.out.println("index peale breaki" + index);
            s = s.substring(index + 1);
            //System.out.println("String peale lapse leidmist " + s);
            if (!s.startsWith(",") || s.length()<=1){
               //System.out.println("Väljastan naabrita lapse:");
               root.tooString();
               return root;
            } else {
               token = ",";
               s = s.substring(1);
             }
         }

         if (token.equals(",")){

            if (testBalance(s)>=0) throw new RuntimeException( s + "Comma in wrong place, next sibling will be independent root!");
            TreeNode parsed = TreeNode.parsePrefix(p+s);
            root.setNextSibling(parsed);
            //System.out.println("Väljastan naabri:");
            root.tooString();
            return root;

            //return new TreeNode(root.name, TreeNode.parsePrefix(s), TreeNode.parsePrefix(s));
         }
         if (token.equals(")")) {
            //System.out.println("Väljastan lehe: ");
            root.tooString();
            return root;
         }
      }
      //throw new RuntimeException();
      return null;
   }

   private static int testBalance(String s) {
      int balance = 0;
      for (int i = 0; i < s.length(); i++) {
         if (s.charAt(i)==')') balance--;
         if (s.charAt(i)=='(') balance++;
         System.out.println(balance);
      }
      return balance;
   }

   public String rightParentheticRepresentation() {
      StringBuffer b = new StringBuffer();
      b.append(this.name);

      if (this.hasNextSibling()){
         b.append(",").append(this.nextSibling.rightParentheticRepresentation());
      }
      if (this.hasFirstChild()){
         b.insert(0, ")");
         b.insert(0, this.firstChild.rightParentheticRepresentation());
         b.insert(0, "(");
      }

      /*if (this.hasFirstChild()) {
         b.append("(");
         b.append(this.firstChild.rightParentheticRepresentation());
         System.out.println(" ess" + b.toString());

      }
      if (this.hasNextSibling()) {
         b.append(name).append(",");
         b.append(this.nextSibling.rightParentheticRepresentation());
         System.out.println("tess" + b.toString());
         //b.append(")");
      } else {
         b.append(this.name).append(")");
      }*/


      return b.toString();
   }

   public void tooString() {
      String firstChildName, nextSiblingName;
      if (!this.hasFirstChild())firstChildName = "null";
      else firstChildName = this.firstChild.name;

      if (!this.hasNextSibling())nextSiblingName = "null";
      else nextSiblingName = this.nextSibling.name;

      System.out.println(name +" "+ firstChildName +" "+ nextSiblingName);
   }

   private boolean hasNextSibling() {
      return !(this.nextSibling==null);
   }

   private boolean hasFirstChild() {
      return !(this.firstChild==null);
   }


   public static void main (String[] param) {
      String s = "A(B1,C,D)";
      s = "A(B(D(G,H),E,F(I)),C(J))";
      //s = "A(B),C(D)";
      //s = "\t";
      TreeNode t = TreeNode.parsePrefix (s);
      t.tooString();

      //System.out.println("Ruudu nimi: " + t.name + ", " + t.firstChild.name);
       //      " Ruudu esimese lapse esimese lapse nimi: " + t.firstChild.firstChild.name);
      String v = t.rightParentheticRepresentation();
      System.out.println (s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
   }


}

